/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vendaland.finch.util;

/**
 *
 * @author root
 */
public class SmsResponse {

    SmsResponses responseCode;
    String responseMessage;

    public SmsResponses getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(SmsResponses responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    @Override
    public String toString() {
        return "SmsResponse{" + "responseCode=" + responseCode + ", responseMessage=" + responseMessage + '}';
    }

}
