package com.vendaland.finch;

import java.io.File;
import java.util.Properties;
import javax.inject.Inject;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import org.flywaydb.core.Flyway;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 *
 * @author vhusha
 */
@Configuration
@EnableJpaRepositories(basePackages = {
    "com.vendaland.finch.repository"
})
@EnableTransactionManagement
@PropertySource("file:${COM_VENDALAND_FINCH_DB_CONFIG_FILE}")
public class DataSourceConfig {

    private static final Logger logger = LoggerFactory.getLogger(DataSourceConfig.class);

    @Autowired
    private ApplicationContext appContext;

    @Inject
    private Environment env;

    Properties additionalProperties() {
        Properties properties = new Properties();
        properties.setProperty("hibernate.dialect", "org.hibernate.dialect.H2Dialect");
        properties.setProperty("hibernate.show_sql", "false");
        properties.setProperty("hibernate.format_sql", "true");
        properties.setProperty("hibernate.enable_lazy_load_no_trans", "true");
        properties.setProperty("spring.jpa.hibernate.naming-strategy", "org.hibernate.cfg.ImprovedNamingStrategy");
        return properties;
    }

    @Autowired
    private DataSource dataSource;

    @Bean(name = "dataSource")
    public DataSource dataSource() {

        DriverManagerDataSource ds = new DriverManagerDataSource();

        ds.setDriverClassName(env.getProperty("db.driver"));
        ds.setUrl(env.getProperty("db.url"));
        ds.setUsername(env.getProperty("db.username"));
        ds.setPassword(env.getProperty("db.password"));

        System.err.println("Added main dataSource");
        return ds;
    }

    @Bean(name = "flyway")
    public Flyway flyway() throws Exception {

        File dbMigrationDir = new File(getClass().getClassLoader().getResource(env.getProperty("db.migration.location")).toURI());

        Flyway fw = new Flyway();
        fw.setLocations("filesystem:" + dbMigrationDir.getAbsolutePath());
        fw.setBaselineOnMigrate(true);
        fw.setTargetAsString(env.getProperty("db.migration.version"));
        fw.setSqlMigrationSuffix(".sql");  // Migrate .sql files
        fw.setDataSource(dataSource);
        try {
            fw.migrate();
        } catch (Exception e) {
            fw.repair();
            fw.migrate();
        }
        System.out.println("flyway bean initialized");
        return fw;
    }

    @Bean(name = "tokenStore")
    @DependsOn("dataSource")
    public TokenStore tokenStore() {
        System.out.println("Zample");
        return new JdbcTokenStore(dataSource);
    }

    @Bean(name = "clientAppDataSource")
    public DataSource clientAppDataSource() {

        DriverManagerDataSource ds = new DriverManagerDataSource();

        ds.setDriverClassName(env.getProperty("client_app.db.driver"));
        ds.setUrl(env.getProperty("client_app.db.url"));
        ds.setUsername(env.getProperty("client_app.db.username"));
        ds.setPassword(env.getProperty("client_app.db.password"));

        System.err.println("Added client app dataSource");
        return ds;
    }

    @Bean
    @DependsOn("flyway")
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(dataSource());
        // Where to look for our entities
        em.setPackagesToScan(new String[]{
            "com.vendaland.finch.entity"
        });

        JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
        em.setJpaProperties(additionalProperties());

        return em;
    }

    @Bean
    JpaTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory);
        return transactionManager;
    }

}
