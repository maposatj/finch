/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vendaland.finch.controller;

import com.google.gson.Gson;
import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author root
 */
@RestController
public class GuestbookController {

    private static final Logger logger = LoggerFactory.getLogger(GuestbookController.class);

    @MessageMapping("/guestbook")
    @SendTo("/topic/entries")
    public String guestbook(String message) {
        logger.info("Received message: {}", message);
        Map<String, String> doi = new HashMap<>();
        doi.put("someMessage", message);
        return new Gson().toJson(doi);
    }
}
