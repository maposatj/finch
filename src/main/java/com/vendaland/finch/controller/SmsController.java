/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vendaland.finch.controller;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.vendaland.finch.entity.BaseModel;
import com.vendaland.finch.model.SmsModel;
import com.vendaland.finch.service.SmsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author root
 */

@RequestMapping("/sms")
@RestController
public class SmsController {

    @Autowired
    SmsService smsService;

    private static final Logger logger = LoggerFactory.getLogger(SmsController.class);

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public String sendSms(@RequestBody BaseModel<SmsModel> model) throws Exception {
        SmsModel smsModel = model.getData();

        logger.info("sendSms requestBody: " + model);

        System.out.println(new Gson().toJson(smsModel));
        

        return smsService.sendSms(smsModel);
    }

    @ResponseBody
    @RequestMapping(value = "/feature1")
    public String feature1() {
        JsonObject rp = new JsonObject();
        rp.addProperty("DEPLOY_QUEUED", "OK");
        return rp.toString();
    }
}

