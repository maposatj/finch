/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vendaland.finch.controller;

import com.google.gson.Gson;
import com.vendaland.finch.entity.Profile;
import com.vendaland.finch.service.UserProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author root
 */
@RestController
@RequestMapping(value = "/profile")
public class UserProfileController {

    @Autowired
    UserProfileService userProfileService;

    @RequestMapping(method = RequestMethod.PUT)
    public String updateProfile() {
        return null;
    }

    @RequestMapping(value = "/me", method = RequestMethod.POST)
    public String selfProfile(OAuth2Authentication auth) {
        User u = (User) auth.getUserAuthentication().getPrincipal();
        Profile P = userProfileService.findByUserId(u.getUsername());
        String d = new Gson().toJson(P);
        return d;
    }
}
