/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vendaland.finch.service;

import com.vendaland.finch.entity.Configuration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import com.vendaland.finch.repository.ConfigurationRepository;

/**
 *
 * @author vhusha
 */
@Service
@Repository
public class ConfigurationService {

    @Autowired
    ConfigurationRepository configurationRepository;

    public Configuration findOne(String key) {
        return configurationRepository.findByKey(key);
    }

    public String findValue(String key) {
        Configuration cfg = configurationRepository.findByKey(key);
        return cfg != null ? cfg.getValue() : null;
    }
}
