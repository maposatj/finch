/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vendaland.finch.service;

import com.google.gson.Gson;
import com.vendaland.finch.entity.BaseResponseModel;
import com.vendaland.finch.model.SmsModel;
import com.vendaland.finch.util.ClickatellSmsProvider;
import com.vendaland.finch.util.SmsResponse;
import com.vendaland.finch.util.SmsResponses;
import java.util.Properties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

/**
 *
 * @author root
 */
@Service
@Repository
public class SmsService {

    @Autowired
    ConfigurationService configurationService;

    public String sendSms(SmsModel smsModel) throws Exception {
        String token = configurationService.findValue("clickatell.token");
        String host = configurationService.findValue("clickatell.host");

        Properties properties = new Properties();
        properties.setProperty("token", token);
        properties.setProperty("host", host);

        ClickatellSmsProvider smsProvider = new ClickatellSmsProvider(properties);

        SmsResponse smsResponse = smsProvider.sendMessage(smsModel);

        BaseResponseModel rm = new BaseResponseModel();
        if (smsResponse.getResponseCode().equals(SmsResponses.SMS_OK)) {
            rm.setResponseCode("0");
        } else {
            rm.setResponseCode("100");
        }
        System.out.println("theResponse: " + smsResponse);
        rm.setResponseMessage(smsResponse.getResponseMessage());

        return new Gson().toJson(rm);
    }
}
