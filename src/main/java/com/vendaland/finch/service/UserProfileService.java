/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vendaland.finch.service;

import com.vendaland.finch.entity.Profile;
import com.vendaland.finch.entity.User;
import com.vendaland.finch.repository.UserProfileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

/**
 *
 * @author vhusha
 */
@Service
@Repository
public class UserProfileService {

    @Autowired
    UserProfileRepository userProfileRepository;
    
    @Autowired
    private UserService userService;

    public Profile findByUserId(String username) {
        User u = userService.loadUserByUsername(username);
        Profile P = userProfileRepository.findByUserId(u.getUserId());
        return P;
    }
}
