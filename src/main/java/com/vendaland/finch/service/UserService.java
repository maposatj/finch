package com.vendaland.finch.service;

import com.vendaland.finch.entity.User;
import com.vendaland.finch.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component("userService")
public class UserService {

    private final Logger log = LoggerFactory.getLogger(UserService.class);

    @Autowired
    private UserRepository userRepository;

    @Transactional
    public User loadUserByUsername(final String username) {
        return userRepository.findByUsernameCaseInsensitive(username);
    }
}
