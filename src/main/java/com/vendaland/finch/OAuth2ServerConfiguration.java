/*
 * Copyright 2014-2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.vendaland.finch;

import com.vendaland.finch.service.UserDetailsService;
import com.vendaland.finch.service.UserService;
import java.util.HashMap;
import javax.sql.DataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.error.OAuth2AuthenticationEntryPoint;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;

@Configuration
public class OAuth2ServerConfiguration {

    private static final String RESOURCE_ID = "restservice";
    private static final Logger logger = LoggerFactory.getLogger(OAuth2ServerConfiguration.class);

    @Configuration
    @EnableResourceServer
    protected static class ResourceServerConfiguration extends
            ResourceServerConfigurerAdapter {

        @Override
        public void configure(ResourceServerSecurityConfigurer resources) {
            // @formatter:off
            resources
                    .resourceId(RESOURCE_ID);
            System.out.println("ResourceServerConfiguration->configure[ResourceServerSecurityConfigurer]");
            // @formatter:on
        }

        @Override
        public void configure(HttpSecurity http) throws Exception {
            // @formatter:off
            http
//                    .addFilterAfter(new Filter() {
//
//                        @Override
//                        public void init(FilterConfig filterConfig) throws ServletException {
//                        }
//
//                        public void doFilter(ServletRequest request, ServletResponse response,
//                                FilterChain chain) throws IOException, ServletException {
//                            ResettableStreamHttpServletRequest wrappedRequest = new ResettableStreamHttpServletRequest(
//                                    (HttpServletRequest) request);
//                            // wrappedRequest.getInputStream().read();
//                            StringBuilder headerString = new StringBuilder("request headers: ");
//                            headerString.append("\n");
//                            Enumeration headerNames = wrappedRequest.getHeaderNames();
//                            String tumble = "";
//                            while (headerNames.hasMoreElements()) {
//                                String headerName = headerNames.nextElement().toString();
//                                headerString.append(tumble);
//                                headerString.append(headerName + ": " + wrappedRequest.getHeader(headerName));
//                                tumble = "\n";
//                            }
//                            String body = IOUtils.toString(wrappedRequest.getReader());
//                            logger.info(headerString.toString());
//                            logger.info("request body: " + body);
////                            auditor.audit(wrappedRequest.getRequestURI(), wrappedRequest.getUserPrincipal(), body);
////                            wrappedRequest.resetInputStream();
//                            chain.doFilter(wrappedRequest, response);
//
//                        }
//
//                        @Override
//                        public void destroy() {
//                        }
//                    }, ChannelProcessingFilter.class)
                    .exceptionHandling()
                    .authenticationEntryPoint(new OAuth2AuthenticationEntryPoint())
                    .and()
                    .logout()
                    .logoutUrl("/oauth/logout")
                    .and()
                    .authorizeRequests()
                    .antMatchers("@PreAuthorize")
                    //                    .antMatchers("/**").permitAll()
                    //                    .antMatchers("/greeting")
                    .authenticated();
            System.out.println("ResourceServerConfiguration->configure[HttpSecurity]");
            // @formatter:on
        }
    }

    @Configuration
    @EnableAuthorizationServer
    protected static class AuthorizationServerConfiguration extends
            AuthorizationServerConfigurerAdapter {

        @Autowired
        private DataSource dataSource;

        @Autowired
        private DataSource clientAppDataSource;

        @Bean
        public TokenStore tokenStore() {
            return new JdbcTokenStore(dataSource);
        }

        @Autowired
        private ApplicationContext appContext;

        @Autowired
        @Qualifier("authenticationManagerBean")
        private AuthenticationManager authenticationManager;

        @Autowired
        private UserDetailsService userDetailsService;

        @Autowired
        UserService userService;

        @Override
        public void configure(AuthorizationServerEndpointsConfigurer endpoints)
                throws Exception {
            // @formatter:off
            System.out.println("AuthorizationServerConfiguration->configure[AuthorizationServerEndpointsConfigurer]");
            endpoints
                    .tokenEnhancer(new TokenEnhancer() {

                        @Override
                        public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication auth) {
                            DefaultOAuth2AccessToken tempResult = (DefaultOAuth2AccessToken) accessToken;
                            final com.vendaland.finch.entity.User u = userService.loadUserByUsername(((org.springframework.security.core.userdetails.User) auth.getUserAuthentication().getPrincipal()).getUsername());
                            tempResult.setAdditionalInformation(new HashMap<String, Object>() {
                                {
                                    put("userId", u.getUserId());
                                }
                            });
                            OAuth2AccessToken result = tempResult;
                            logger.info("token enhanced: {}", result.getValue());
                            return result;
                        }
                    })
                    .tokenStore(appContext.getBean("tokenStore", TokenStore.class))
                    .authenticationManager(authenticationManager)
                    .userDetailsService(userDetailsService);
//                    .userDetailsService(userDetailsService);
            // @formatter:on
        }

        @Override
        public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
            // @formatter:off
            clients.jdbc(clientAppDataSource);
//            clients
//                    .inMemory()
//                    .withClient("clientapp")
//                    .authorizedGrantTypes("password", "refresh_token")
////                    .authorities("USER")
//                    .scopes("read", "write")
//                    .resourceIds(RESOURCE_ID)
//                    .secret("secret2");
            // @formatter:on
        }

        @Bean
        @Primary
        public DefaultTokenServices tokenServices() {
            DefaultTokenServices tokenServices = new DefaultTokenServices();
            tokenServices.setSupportRefreshToken(true);
            tokenServices.setTokenStore(appContext.getBean("tokenStore", TokenStore.class));
            return tokenServices;
        }

    }

}
