ALTER TABLE `user_authority` DROP FOREIGN KEY `user_authority_ibfk_1`;
ALTER TABLE `user` DROP PRIMARY KEY;
ALTER TABLE `user` ADD `user_id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT FIRST, AUTO_INCREMENT=1;
ALTER TABLE `user` ADD CONSTRAINT username_unq UNIQUE (username);
ALTER TABLE `user_authority`
  ADD CONSTRAINT `user_authority_ibfk_1` FOREIGN KEY (`username`) REFERENCES `user` (`username`);

create table personal_details(
    id int(11) auto_increment not null,
    user_id_FK int not null,
    first_name varchar(100) not null,
    last_name varchar(100) null,
    date_of_birth int null,
    cell_number varchar(10) null,
    country_code varchar(2) null,
    gender varchar(1) null,
    primary key(id),
    foreign key (user_id_FK) references `user`(user_id)
);
INSERT INTO `personal_details`(
    `user_id_FK`, 
    `first_name`, 
    `last_name`)  
SELECT `user_id`, '', '' FROM `user`;

